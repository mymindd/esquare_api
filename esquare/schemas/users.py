def user_entity(user) -> dict:
    return {
        "id": str(user["_id"]),
        "first_name": user["first_name"],
        "last_name": user["last_name"],
        "email_address": user["email_address"],
        "password": user["password"],
    }
