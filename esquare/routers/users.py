from fastapi import APIRouter
from esquare.models.users import User

router = APIRouter(
    prefix="/users",
    tags=["users"],
)

@router.get('/')
async def index():
    return {"status": "API for users"}

